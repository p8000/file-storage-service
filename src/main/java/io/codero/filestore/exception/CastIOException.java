package io.codero.filestore.exception;

public class CastIOException extends RuntimeException {
    public CastIOException(String message) {
        super(message);
    }
}
