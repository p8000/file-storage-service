CREATE TABLE file(
    id UUID PRIMARY KEY,
    file_name VARCHAR(500)
);